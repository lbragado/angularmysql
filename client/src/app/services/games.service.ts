import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  //Importamos para poder usar peticiones http

import { Game } from '../models/Game'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GamesService {

  API_URI = 'http://localhost:3000/api';

  constructor( private http: HttpClient) { }

  getGames() {
    console.log(this.API_URI + '/games');
    return this.http.get(this.API_URI + '/games');
  }

  getGame(id: string) {
    //return this.http.get('${this.API_URI}/games/${id}');
    return this.http.get(this.API_URI + '/games/' + id);
  }

  deleteGame(id: string) {
    return this.http.delete(this.API_URI + '/games/' + id);
  }

  //Método que recibe un objeto de tipo interface Game
  saveGame(game: Game) {
    return this.http.post(this.API_URI + '/games', game);
  }

  //id: indicamos que puede ser de tipo string o número
  updateGame(id: string|number, updatedGame: Game): Observable<Game> {
    return this.http.put(this.API_URI + '/games/' + id, updatedGame);
  }

}
