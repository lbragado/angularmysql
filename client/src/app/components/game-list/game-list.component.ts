import { Component, OnInit, HostBinding } from '@angular/core';

//Importamos el servicio
import { GamesService } from '../../services/games.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  //Agregamos una fila a todo el componente para que los componentes estén en columnas en esta fila creada
  @HostBinding('class') classes = 'row';

  games: any = [];
  constructor(private gamesService: GamesService) { }

  ngOnInit() {
    this.getGames();
  }

  getGames() {
    this.gamesService.getGames().subscribe(
      res => {
        this.games = res;
      },
      err => console.log(err)
    );
  }

  deleteGame(id:string) {
    this.gamesService.deleteGame(id).subscribe(
      res => {
        console.log(res);
        this.getGames();
      },      
      err => console.log(err)
    );
  }

  //No ocuparemos este método ya que desde la página lo estamos redireccionando directamente
  // editGame(id:string) {
  //   console.log(id);
  // }
}
