import express, { Application } from "express"; 
import indexRoutes from './routes/indexRoutes';
import gamesRoutes from './routes/gamesRoutes';
import morgan from 'morgan';
import cors from 'cors';


class Server{
    
    public app:Application;

    constructor(){
        this.app = express();
        this.config();
        this.routes();
    }

    config():void{
        //Con process.env.PORT indicamos que si en la máquina tenemos un puerto lo usemos de lo contrario usaremos el que estemos indicando, en este caso el 3000
        this.app.set("port", process.env.PORT || 3000); 
        //Indicamos que usaremos morgan para ver quién hace peticiones al servidor
        this.app.use(morgan('dev'));
        //Indicamos que usaremos cors para hacer peticiones al servidor 
        this.app.use(cors());
        //Indicamos que aceptaremos formatos json, es decir, que el servidor podrá aceptar formatos json
        this.app.use(express.json());
        //En caso de que queramos enviar desde un formulario html informaicón
        this.app.use(express.urlencoded({extended:false}));

    }

    // Método para definir las rutas de mi servidor
    routes():void{
        this.app.use('/',indexRoutes);
        this.app.use('/api/games',gamesRoutes);
    }

    //Método para inicializar el servidor
    start():void{
        this.app.listen(this.app.get('port'), ()=>{
            console.log('Server on port ', this.app.get('port'));
        });            
    }
}

//Arriba ya configuramos ahora generaremos un servidor
const server = new Server();
server.start();

//console.log('Works2.3!');