"use strict";
exports.__esModule = true;
var IndexController = /** @class */ (function () {
    function IndexController() {
    }
    IndexController.prototype.index = function (req, res) {
        res.send('Hello Index!!!');
    };
    return IndexController;
}());
exports.indexController = new IndexController();
exports["default"] = exports.indexController;
