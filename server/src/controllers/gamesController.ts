import { Request, Response } from 'express';
import pool from '../database'  //database es el archivo database.ts

class GamesController{
    
    public async list(req: Request, res: Response) {
        const games = await pool.query('SELECT * FROM games');        
        res.json(games);
    }

    public async getOne(req: Request, res:Response):Promise<any>{
        const { id} = req.params;
        const games = await pool.query('SELECT * FROM games WHERE id=?', [id]);
        console.log(games);

        if(games.length > 0){
            return res.json(games[0]);
        }
        res.status(404).json({text: "The game doesn't exists"});
    }

    public async create(req: Request, res: Response): Promise<void>{
        await pool.query('INSERT INTO games set ?', [req.body]);
        res.json({text: 'Game saved'});
    }

    public async delete (req: Request, res:Response):Promise<void>{
        const { id} = req.params;
        await pool.query('DELETE FROM games WHERE id=?', [id])
        res.json({text: 'The game was deleted'});        
    }

    public async update (req: Request, res:Response):Promise<void>{
         const {id} = req.params;
         await pool.query('UPDATE games SET ? WHERE id = ?', [req.body, id])
         res.json({text: 'The game was updated ok!'});
    }
    
}

export const gamesController = new GamesController();
export default gamesController;