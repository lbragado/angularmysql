"use strict";
exports.__esModule = true;
var express_1 = require("express");
var indexRoutes_1 = require("./routes/indexRoutes");
var gamesRoutes_1 = require("./routes/gamesRoutes");
var morgan_1 = require("morgan");
var cors_1 = require("cors");
var Server = /** @class */ (function () {
    function Server() {
        this.app = express_1["default"]();
        this.config();
        this.routes();
    }
    Server.prototype.config = function () {
        //Con process.env.PORT indicamos que si en la máquina tenemos un puerto lo usemos de lo contrario usaremos el que estemos indicando, en este caso el 3000
        this.app.set("port", process.env.PORT || 3000);
        //Indicamos que usaremos morgan para ver quién hace peticiones al servidor
        this.app.use(morgan_1["default"]('dev'));
        //Indicamos que usaremos cors para hacer peticiones al servidor 
        this.app.use(cors_1["default"]());
        //Indicamos que aceptaremos formatos json, es decir, que el servidor podrá aceptar formatos json
        this.app.use(express_1["default"].json());
        //En caso de que queramos enviar desde un formulario html informaicón
        this.app.use(express_1["default"].urlencoded({ extended: false }));
    };
    // Método para definir las rutas de mi servidor
    Server.prototype.routes = function () {
        this.app.use('/', indexRoutes_1["default"]);
        this.app.use('/api/games', gamesRoutes_1["default"]);
    };
    //Método para inicializar el servidor
    Server.prototype.start = function () {
        var _this = this;
        this.app.listen(this.app.get('port'), function () {
            console.log('Server on port ', _this.app.get('port'));
        });
    };
    return Server;
}());
//Arriba ya configuramos ahora generaremos un servidor
var server = new Server();
server.start();
//console.log('Works2.3!');
