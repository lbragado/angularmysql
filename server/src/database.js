"use strict";
exports.__esModule = true;
var mysql = require("promise-mysql");
var keys_1 = require("./keys");
var pool = mysql.createPool(keys_1["default"].database);
pool.getConnection().then(function (connection) {
    pool.releaseConnection(connection);
    console.log('DB is connected');
});
exports["default"] = pool;
