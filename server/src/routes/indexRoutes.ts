import { Router } from 'express';

import indexController from '../controllers/indexController';

class IndexRoutes {

    public router : Router = Router();

    constructor(){
        this.config();
    }

    config(): void {
        //Configuramos la ruta por default de nuestra aplicación, cuando consulten esta ruta inicial devolveremos el mensaje
        //this.router.get('/', (req, res) => res.send('Hello!!!')); //Comentamos esta línea ya que separamos el código y ahora está en el Controller
        this.router.get('/', indexController.index);
    }
}

const indexRoute = new IndexRoutes();
export default indexRoute.router;