import { Router } from 'express';

import gamesController from '../controllers/gamesController';

class GamesRoutes {

    public router : Router = Router();

    constructor(){
        this.config();
    }

    config(): void {
        //Configuramos la ruta por default de nuestra aplicación, cuando consulten esta ruta inicial devolveremos el mensaje
        this.router.get('/', gamesController.list);
        this.router.get('/:id', gamesController.getOne);
        this.router.post('/', gamesController.create);
        this.router.delete('/:id', gamesController.delete)
        this.router.put('/:id', gamesController.update)
    }
    
}

const gameRoute = new GamesRoutes();
export default gameRoute.router;