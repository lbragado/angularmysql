"use strict";
exports.__esModule = true;
var express_1 = require("express");
var gamesController_1 = require("../controllers/gamesController");
var GamesRoutes = /** @class */ (function () {
    function GamesRoutes() {
        this.router = express_1.Router();
        this.config();
    }
    GamesRoutes.prototype.config = function () {
        //Configuramos la ruta por default de nuestra aplicación, cuando consulten esta ruta inicial devolveremos el mensaje
        this.router.get('/', gamesController_1["default"].list);
        this.router.get('/:id', gamesController_1["default"].getOne);
        this.router.post('/', gamesController_1["default"].create);
        this.router["delete"]('/:id', gamesController_1["default"]["delete"]);
        this.router.put('/:id', gamesController_1["default"].update);
    };
    return GamesRoutes;
}());
var gameRoute = new GamesRoutes();
exports["default"] = gameRoute.router;
