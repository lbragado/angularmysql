"use strict";
exports.__esModule = true;
var express_1 = require("express");
var indexController_1 = require("../controllers/indexController");
var IndexRoutes = /** @class */ (function () {
    function IndexRoutes() {
        this.router = express_1.Router();
        this.config();
    }
    IndexRoutes.prototype.config = function () {
        //Configuramos la ruta por default de nuestra aplicación, cuando consulten esta ruta inicial devolveremos el mensaje
        //this.router.get('/', (req, res) => res.send('Hello!!!')); //Comentamos esta línea ya que separamos el código y ahora está en el Controller
        this.router.get('/', indexController_1["default"].index);
    };
    return IndexRoutes;
}());
var indexRoute = new IndexRoutes();
exports["default"] = indexRoute.router;
