"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const indexRoutes_1 = __importDefault(require("./routes/indexRoutes"));
const gamesRoutes_1 = __importDefault(require("./routes/gamesRoutes"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    config() {
        //Con process.env.PORT indicamos que si en la máquina tenemos un puerto lo usemos de lo contrario usaremos el que estemos indicando, en este caso el 3000
        this.app.set("port", process.env.PORT || 3000);
        //Indicamos que usaremos morgan para ver quién hace peticiones al servidor
        this.app.use(morgan_1.default('dev'));
        //Indicamos que usaremos cors para hacer peticiones al servidor 
        this.app.use(cors_1.default());
        //Indicamos que aceptaremos formatos json, es decir, que el servidor podrá aceptar formatos json
        this.app.use(express_1.default.json());
        //En caso de que queramos enviar desde un formulario html informaicón
        this.app.use(express_1.default.urlencoded({ extended: false }));
    }
    // Método para definir las rutas de mi servidor
    routes() {
        this.app.use('/', indexRoutes_1.default);
        this.app.use('/api/games', gamesRoutes_1.default);
    }
    //Método para inicializar el servidor
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server on port ', this.app.get('port'));
        });
    }
}
//Arriba ya configuramos ahora generaremos un servidor
const server = new Server();
server.start();
//console.log('Works2.3!');
