"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const indexController_1 = __importDefault(require("../controllers/indexController"));
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        //Configuramos la ruta por default de nuestra aplicación, cuando consulten esta ruta inicial devolveremos el mensaje
        //this.router.get('/', (req, res) => res.send('Hello!!!')); //Comentamos esta línea ya que separamos el código y ahora está en el Controller
        this.router.get('/', indexController_1.default.index);
    }
}
const indexRoute = new IndexRoutes();
exports.default = indexRoute.router;
